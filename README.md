# QuizBot
Experimental discord bot to, well, quiz people

This bot is in private beta at the moment, so the source code is not available. However, I will be using GitHub to host the [documentation of the QuizBot JSON file format](https://gitlab.com/nonnymoose/QuizBot/wikis) and the [GUI quiz creator](https://nonnymoose.gitlab.io/QuizBot), for those who are involved in the private beta.
